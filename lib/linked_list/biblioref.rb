class Biblioref
  attr_accessor :autors
      attr_accessor :title
      attr_accessor :series
      attr_accessor :p_house #Casa editorial
      attr_accessor :edit_num #Numero de edición
      attr_accessor :p_date #Fecha de publicación
      attr_accessor :isbn_num #Números isbn
      def initialize(params = {})
          @autors = []
          if params.fetch(:author).class.to_s=="Array"
            for i in 0..params.fetch(:author).count-1
            @autors << params.fetch(:author)[i]
          end
          else
            @autors << params.fetch(:author)
          end
          @title = params.fetch(:title)
          @series= params.fetch(:series, "")
          @p_house = params.fetch(:p_house)
          @edit_num = params.fetch(:edit_number)
          @p_date = params.fetch(:p_date)
          @isbn_num = []
          if params.fetch(:isbn_num).class.to_s=="Array"
            for i in 0..params.fetch(:isbn_num).count-1
            @isbn_num << params.fetch(:isbn_num)[i]
          end
        else
          @isbn_num << params.fetch(:isbn_num)
          end
          end
      def to_s
          names =""
          isbns =""
          i=0
  while i < @autors.count
if i != @autors.count-1
  names= names + "#{@autors[i]}, "
else
  names= names + "#{@autors[i]}."
end
  i=i+1
end
i=0
while i < @isbn_num.count
  isbns= isbns + "#{@isbn_num[i]}\n"
  i=i+1
end
if series != ""
      "#{names}\n#{@title}\n(#{@series})\n#{@p_house}; #{@edit_num} edition (#{@p_date})\n#{isbns}"
else
      "#{names}\n#{@title}\n#{@p_house}; #{@edit_num} edition (#{@p_date})\n#{isbns}"
    end
    end 
end

