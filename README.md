Lenguajes y Paradigmas de Programación
==================


Práctica 7 - Programación Orientada a Objetos
-----------

Autor:

* Mauricio Orta

Descripción
----------------------

Este repositorio contiene un árbol de directorios y ficheros creados con la herramienta Bundler, la cual organiza la estructura necesaria para 
poder obtener una "gema" o librería de ruby.

Se incluyen dos clases en lib: una Biblioref, para el almacenamiento y gestión de referencias bibliográficas creada en la práctica anterior, y 
una clase List en list.rb con los atributos y métodos necesarios para emular el comportamiento de una lista enlazada simple.

Igualmente, existe un fichero de expectativas linked_list_spec.rb en el directorio spec, el cual contiene grupos de ejemplos de expectativas
que se fueron escribiendo a medida que se desarrollaba el código para verificar y evaluar su funcionamiento.

Árbol de ficheros y directorios
-------------------------------
``` 
.
├── CODE_OF_CONDUCT.md
├── Gemfile
├── LICENSE.txt
├── README.md
├── Rakefile
├── bin
│   ├── console
│   └── setup
├── lib
│   ├── linked_list
│   │   ├── biblioref.rb
│   │   ├── list.rb
│   │   └── version.rb
│   └── linked_list.rb
├── linked_list.gemspec
└── spec
    ├── linked_list_spec.rb
    └── spec_helper.rb
``` 
    
---------------------------