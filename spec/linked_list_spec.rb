require 'spec_helper'

describe LinkedList do
  before:each do
    @nodo1= Biblioref.new(:author => ["Dave Thomas", "Andy Hunt", "Chad Fowler"], :title => "Programming Ruby 1.9 & 2.0: The Pragmatic Programmers Guide", :series => "The Facets of Ruby", :p_house => "Pragmatic Bookshelf",:edit_number => 4, :p_date => "July 7, 2013", :isbn_num => ["ISBN-13: 978-1937785499", "ISBN-10: 1937785491"])
    @nodo2= Biblioref.new(:author => "Scott Chacon",:title => "Pro Git 2009th Edition",  :series => "Pro", :p_house => "Apress", :edit_number => 2009, :p_date => "August 27, 2009", :isbn_num => ["ISBN-13: 978-1430218333", "ISBN-10: 1430218339"])
    @nodo3= Biblioref.new(:author => ["David Flanagan", "Yukihiro Matsumoto"],  :title => "The Ruby Programming Language", :p_house => "O’Reilly Media",  :edit_number => 1, :p_date => "February 4, 2008", :isbn_num => ["ISBN-10: 0596516177", "ISBN-13: 978-0596516178"])
    @nodo4 = Biblioref.new(:author => ["David Chelimsky", "Dave Astels", "Bryan Helmkamp", "Dan North", "Zach Dennis", "Aslak Helleso"], :title => "The RSpec Book: Behaviour Driven Development with RSpec,  Cucumber, and Friends", :series => "The Facets of Ruby", :p_house => "Pragmatic Bookshelf", :edit_number => 1, :p_date => "December 25, 2010", :isbn_num => ["SBN-10: 19343563
79", "ISBN-13: 978-1934356371"])
    @nodo5=  Biblioref.new(:author => "Richard E. Silverman", :title => "Git Pocket Guide", :p_house => "O’Reilly Media", :edit_number => 1, :p_date => "August 2, 2013", :isbn_num => ["ISBN-10: 1449325866", "ISBN-13: 978-1449325862"])
    @lista = List.new(@nodo1)
  end
  
  describe  "Lista" do
    
    it "Toma un objeto y retorna un objeto de clase List" do
    expect(@lista).to be_a List
  end

  
  it "Toma un primer objeto y lo guarda como un nodo de inicio/head y final de la lista" do
    expect(@lista.head["value"]).to be @nodo1
    expect(@lista.end_["value"]).to be @nodo1
    expect(@lista.head["next"]).to be nil
    expect(@lista.end_["next"]).to be nil
  end
  
  it "Se puede extraer el primer nodo de la lista" do
  expect(@lista.extract_head).to be @nodo1
end

it "Se puede insertar un objeto como un nuevo nodo al inicio de la lista" do
  @lista.insert_h(@nodo2)
  expect(@lista.head["value"]).to be @nodo2
  expect(@lista.head["next"]["value"]).to be @nodo1
end

it "Se pueden insertar varios objetos a la vez como nuevos nodos al inicio de la lista" do
  @lista.insert_h([@nodo2, @nodo3])
  expect(@lista.head["value"]).to be @nodo3
  expect(@lista.head["next"]["value"]).to be @nodo2
  expect(@lista.head["next"]["next"]["value"]).to be @nodo1
  
end

it "Se puede extraer el nodo final de la lista" do
  @lista.insert_h([@nodo2, @nodo3])
  expect(@lista.extract_end).to be @nodo1
end

it "Se puede insertar un objeto como un nuevo nodo al final de la lista" do
@lista.insert_e(@nodo2)
expect(@lista.head["value"]).to be @nodo1
expect(@lista.head["next"]["value"]).to be @nodo2
end

it "Se pueden insertar varios objetos a la vez como nuevos nodos al final de la lista" do
  @lista.insert_e([@nodo2, @nodo3])
  expect(@lista.head["value"]).to be @nodo1
  expect(@lista.head["next"]["value"]).to be @nodo2
  expect(@lista.head["next"]["next"]["value"]).to be @nodo3
  
end

it "Se puede imprimir la lista" do
  @lista.insert_h([@nodo2, @nodo3, @nodo4, @nodo5])
  expect(@lista.to_s).to be == "[#{@nodo5}]--->[#{@nodo4}]--->[#{@nodo3}]--->[#{@nodo2}]--->[#{@nodo1}]"
end

end






    

end
